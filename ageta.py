import numpy as np
from grapa.functionals import *
from grapa.constants import *

from grapa.layers import ghealparam

import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K

from tensorflow.keras.utils import plot_model
from tensorflow.keras.losses import mse
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Lambda, Input

from gaddparam import *


shallvae=False
lr=0.001
epochs=100
batch_size=20
patience=10


nshape=7
dimension=2


np.random.seed(12)
data=np.random.normal(0,1,(1000,nshape))






def createmodel():
  i=Input(shape=data.shape[1:])
  g=grap(state(gs=1,param=nshape))
  g.X=i
 
  m=getm()


  #g=gll(g,m,k=1)
  m.decompress="paramlike"

  g,com,i2=decompress(g,m,[4,4])  #not the best design actually
  #g,com,i2=decompress(g,m,[2,2])  #not the best design actually

  g=gnl(g,m)
  g=gnl(g,m)
  g=gnl(g,m)

  g=remparam(g,dimension)

  ret=ghealparam(gs=g.s.gs,param1=g.s.gs,param2=g.s.param)([g.A,g.X])
  return i2,g.A,g.X,ret


def subloss(x):
  lx=int(x.shape[-1])

  #print(x.shape)
  #exit()

  #return x[:,0]

  loss=None#1/(1+np.sum(x,axis=-1))

  for i in range(lx):
    for j in range(lx):
      if i<=j:continue
      ac=x[:,i]*x[:,j]
      if loss is None:
        loss=ac
      else:
        loss+=ac  



  return loss

def getae():
  i,A,X,out=createmodel()

  s=int(A.shape[-1])

  m=Model(inputs=i,outputs=out)



  loss=None
  divide=None
  for i in range(s):
    for j in range(s):
      if i<=j:continue
      ac=(K.abs(A[:,j,i]))*subloss(K.abs(X[:,i,:]-X[:,j,:]))
      ac=K.abs(ac)
      
      if loss is None:
        loss=ac
        divide=K.abs(A[:,j,i])
      else:
        loss+=ac
        divide+=K.abs(A[:,j,i])

  loss/=divide+0.01
  loss=K.mean(loss)
  
  loss+=K.mean(K.abs(K.mean(K.std(X,axis=1),axis=1)-1),axis=0) 


  m.add_loss(loss)
  m.compile(Adam(lr=lr))
  m.summary()
  plot_model(m,to_file="model.png",show_shapes=True)

  return m

  exit()


  print(i.shape,A.shape,X.shape)

  exit()

  if shallvae:
    z=Lambda(sampling,name='z')([z1,z2])
  else:
    z=z1

  encoder=Model(i1,[z1,z2,z],name="encoder")
  decoder=Model(i2,o,name="decoder")

  plot_model(encoder,to_file="encoder.png",show_shapes=True)
  plot_model(decoder,to_file="decoder.png",show_shapes=True)

  o2=decoder(encoder(i1)[2:])

  vae=Model(i1,o2,name="vae")
  

  loss=mse(c,o2)
  loss=K.mean(loss)

  if shallvae:
    kl_loss=-0.5*K.mean(1+z2-K.square(z1)-K.exp(z2))
    loss+=kl_loss
  
  vae.add_loss(loss)
  vae.compile(Adam(lr=lr))
  vae.summary()
  plot_model(vae,to_file="vae.png",show_shapes=True)

  return vae

